//
//  AppDelegate.h
//  NSLog_printf
//
//  Created by 김정민 on 18/06/2019.
//  Copyright © 2019 김정민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

