//
//  main.m
//  NSLog printf
//
//  Created by 김정민 on 18/06/2019.
//  Copyright © 2019 김정민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import <Foundation/Foundation.h>

@interface Parent : NSObject
@end

@implementation Parent
@end

@interface Child : Parent
@end

@implementation Child
@end

int main(int argc, char * argv[]) {
    @autoreleasepool {
        int iValue = 100;
        float fValue = 331.79;
        double fDouble = 1.44e+1l;
        double gDouble = 1.44e+0l;
        char chValue = 'w';
        
        printf("%d %f %g %g %c\n", iValue, fValue, fDouble, gDouble, chValue);
        // 100 331.790009 14.4 1.44 w
        
        id p, c;
        
        p = [[Parent alloc] init];
        c = [[Child alloc] init];
        NSLog(@"%p %@", p, p); // 0x60000041c250 <Parent: 0x60000041c250>
        NSLog(@"%p %@", c, c); // 0x60000041c240 <Child: 0x60000041c240>
        
        // sizeof() 연사자가 unsigned long을 반환하기때문에 %d 구분자로 표시하기 위해 (int)형으로 cast(캐스트, 강제) 변환한다.
        printf("char : %d\n", (int)sizeof(char)); // 1
        printf("short : %d\n", (int)sizeof(short)); // 2
        printf("int : %d\n", (int)sizeof(int)); // 4
        printf("long : %d\n", (int)sizeof(long)); // 8
        printf("long long : %d\n", (int)sizeof(long long)); // 8
        
        printf("unsigned char : %d\n", (int)sizeof(unsigned char)); // 1
        printf("unsigned short : %d\n", (int)sizeof(unsigned short)); // 2
        printf("unsigned int : %d\n", (int)sizeof(unsigned int)); // 4
        printf("unsigned long : %d\n", (int)sizeof(unsigned long)); // 8
        printf("unsigned long long : %d\n", (int)sizeof(unsigned long long)); // 8
        
        printf("float : %d\n", (int)sizeof(float)); // 4
        printf("double : %d\n", (int)sizeof(double)); // 8
        
        // char, short 형식을 표혀ㄴ하기 위해서는 %h가 더 적당할 것 같으나 #difine의 정의상 int 형식의 구분자를 사용해야 한다.
        printf("CHAR_MIN : %i\n", CHAR_MIN); // -128
        printf("CHAR_MAX : %i\n", CHAR_MAX); // 127
        printf("SHRT_MIN : %i\n", SHRT_MIN); // -32768
        printf("SHRT_MAX : %i\n", SHRT_MAX); // 32767
        
        printf("INT_MIN : %i\n", INT_MIN); // -2147483648
        printf("INT_MAX : %i\n", INT_MAX); // 2147483647
        printf("LONG_MIN : %li\n", LONG_MIN); // -9223372036854775808
        printf("LONG_MAX : %li\n", LONG_MAX); // 9223372036854775807
        printf("LLONG_MIN : %lli\n", LLONG_MIN); // -9223372036854775808
        printf("LLONG_MAX : %lli\n", LLONG_MAX); // 9223372036854775807
        
        printf("UCHAR_MAX : %u\n", UCHAR_MAX); // 255
        printf("USHRT_MAX : %u\n", USHRT_MAX); // 65535
        printf("UINT_MAX : %u\n", UINT_MAX); // 4294967295
        printf("ULONG_MAX : %lu\n", ULONG_MAX); // 18446744073709551615
        printf("ULLONG_MAX : %llu\n", ULLONG_MAX); // 18446744073709551615
        
        return 0;
    }
}
