//
//  main.m
//  EmptyStatic
//
//  Created by 김정민 on 19/06/2019.
//  Copyright © 2019 김정민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import <Foundation/Foundation.h>
#import "Empty.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        Empty *a = [[Empty alloc] init];
        Empty *b = [[Empty alloc] init];
        
        [Empty settingStatic:5];
        [Empty settingStatic:4];
        
        NSLog(@"인스턴스 메소드로 클래스 변수 접근 : %d %d", [a gettingStatic], [b gettingStatic]);
        NSLog(@"외부링크 함수로 클래스 변수 접근 : %d", gettingStatic());
        
        settingStatic(6);
        
        NSLog(@"인스턴스 메소드로 클래스 변수 접근 : %d %d", [a gettingStatic], [b gettingStatic]);
        NSLog(@"외부링크 함수로 클래스 변수 접근 : %d", gettingStatic());
        [a release];
        [b release];
        
        return 0;
    }
}
