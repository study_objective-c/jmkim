//
//  Empty.h
//  EmptyStatic
//
//  Created by 김정민 on 19/06/2019.
//  Copyright © 2019 김정민. All rights reserved.
//

#ifndef Empty_h
#define Empty_h

#import <Foundation/Foundation.h>

#endif /* Empty_h */

@interface Empty : NSObject
{
    // interface 구역에는 static 변수 선언 불가
    // static int i;
}
-(int)gettingStatic;
+(void)settingStatic:(int)j;
@end

extern int gettingStatic(void);
extern void settingStatic(int);
