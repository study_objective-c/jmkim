//
//  Empty.m
//  EmptyStatic
//
//  Created by 김정민 on 19/06/2019.
//  Copyright © 2019 김정민. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Empty.h"

//static int s; // 외부일 경우 해당 모듈에서 공유, @implementation 내부와 중복 선언시는 한쪽은 무시.

@implementation Empty
static int s; // @implementation 영역 내외부 위치 가능
-(int)gettingStatic
{
    return s;
}
+(void)settingStatic:(int)j
{
    s = j;
}
@end

int gettingStatic(void)
{
    return s;
}
void settingStatic(int j)
{
    s = j;
}
